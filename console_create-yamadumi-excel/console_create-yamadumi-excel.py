import matplotlib.pyplot as plt
import pathlib
import sys
import pandas as pd
import typing


def calculte_project_yamadumi_rate(months: int, yamadumi_rate: typing.List[float]):
    # タスクの月数が山積み比率の値の個数と異なる場合は比率の調整が必要

    # 配分の1ヶ月分がタスクの何ヶ月分に相当するかを計算
    months_per_pattern = months / len(yamadumi_rate)

    # 山積み比率の値ごとの適用タスク月数をリストとして保持
    pattern_months_left_list = [months_per_pattern for _ in range(len(yamadumi_rate))]

    # 月ごと山積み比率の値ごとの配分を2次元リストとして保持
    pattern_usage_list = []
    for _ in range(months):
        # 月ごとに処理する
        month_left = 1
        pattern_usage = []
        for j in range(len(pattern_months_left_list)):
            # 山積み比率ごとに、その月の何割分にその比率を適用するのかを算出する
            pattern_months_left = pattern_months_left_list[j]
            if month_left <= pattern_months_left:
                # その月の残月数が山積み比率の適用可能月数以下の場合、
                # その月の残月数すべてにその山積み比率を適用する
                pattern_usage.append(month_left)
                # 山積み比率の適用可能月数から適用した月数を減らす
                pattern_months_left_list[j] -= month_left
                # その月の残月数から適用した月数を減らす、つまり0にする
                month_left = month_left - month_left
            else:
                # その月の残月数が山積み比率の適用可能月数より大きい場合、
                # 適用可能月数分にその山積み比率を適用する
                pattern_usage.append(pattern_months_left)
                # 山積み比率の適用可能月数から適用した月数を減らす、つまり0にする
                pattern_months_left_list[j] -= pattern_months_left
                # その月の残月数から適用した月数を減らす
                month_left = month_left - pattern_months_left
        pattern_usage_list.append(pattern_usage)

    # 月ごとの山積み比率を算出する
    project_yamadumi_rate = []
    for pattern_usage in pattern_usage_list:
        rate = 0
        for i in range(len(pattern_usage)):
            rate += pattern_usage[i] * yamadumi_rate[i] / months_per_pattern
        project_yamadumi_rate.append(rate)
    return project_yamadumi_rate

def expand_row_to_yamadumi_df(row: pd.Series, yamadumi_rate: typing.List[float]):
    # タスクの月数を取得する
    months = row["months"]

    # タスクの月ごとの行を作成する
    yamadumi_df_list = [pd.DataFrame([row]).copy() for _ in range(months)]
    yamadumi_df = pd.concat(yamadumi_df_list)

    # タスクの月ごとの行の年月を設定する
    yamadumi_ym = pd.date_range(start=row["start_ym"], periods=months, freq="M")
    yamadumi_df["yamadumi_ym"] = yamadumi_ym

    # タスクの月ごと山積み比率を算出する
    yamadumi_df["yamadumi_rate"] = calculte_project_yamadumi_rate(months=months, yamadumi_rate=yamadumi_rate)

    # タスクの月ごと工数を算出する
    yamadumi_df["yamadumi_man_month"] = yamadumi_df["man_month"] * yamadumi_df["yamadumi_rate"]
    return yamadumi_df

def expand_to_yamadumi_df(df: pd.DataFrame, yamadumi_rate: typing.List[float]):
    yamadumi_df_list = []
    for _, r in df.iterrows():
        yamadumi_df_list.append(expand_row_to_yamadumi_df(row=r, yamadumi_rate=yamadumi_rate))
    return pd.concat(yamadumi_df_list).reset_index(drop=True)

self_file = pathlib.Path(__file__).resolve()
self_folder = self_file.parent
pkg_path = self_folder.parent.resolve()

if str(pkg_path) not in sys.path:
    sys.path.insert(0, str(pkg_path))


input_folder = self_folder.joinpath("input")
output_folder = self_folder.joinpath("output")
yamadumi_excel_file = output_folder.joinpath("yamadumi.xlsx")

# Excelを読み込む
project_data_file = input_folder.joinpath("project_data.xlsx")
project_df = pd.read_excel(io=project_data_file, sheet_name="project")
print(project_df)

# 山積み比率を定義する
yamadumi_rate = [0.1, 0.2, 0.3, 0.4, 0.3, 0.2, 0.1]

# タスクのデータを山積みのデータに変換する
yamadumi_df = expand_to_yamadumi_df(df=project_df, yamadumi_rate=yamadumi_rate)
print(yamadumi_df)

# Excelに出力する
yamadumi_df.to_excel(yamadumi_excel_file, sheet_name="yamadumi", index=False)

# 【おまけ】グラフを表示する
graph_df = yamadumi_df.copy()
graph_df = graph_df.pivot_table(columns=["project", "task"],index="yamadumi_ym", values="yamadumi_man_month")
graph_df.plot.bar(stacked=True, width=1)
plt.show()
