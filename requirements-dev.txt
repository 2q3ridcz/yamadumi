flake8==6.0.0
pylint==2.17.4
pytest==7.3.2
pytest-cov==4.1.0
